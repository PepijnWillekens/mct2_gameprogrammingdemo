﻿using UnityEngine;
using System.Collections;

public class CameraBeh : MonoBehaviour {
    public float speed;

    // Update is called once per frame
    void Update()
    {
        Camera.main.transform.LookAt(Vector3.zero);
        if (Input.GetKey(KeyCode.UpArrow))
        {
            Camera.main.transform.position += Camera.main.transform.up * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            Camera.main.transform.position -= Camera.main.transform.up * speed * Time.deltaTime;
        }


        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Camera.main.transform.position -= Camera.main.transform.right * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            Camera.main.transform.position += Camera.main.transform.right * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.RightControl))
        {
            Camera.main.transform.position -= Camera.main.transform.forward * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.RightShift))
        {
            Camera.main.transform.position += Camera.main.transform.forward * speed * Time.deltaTime;
        }
    }

}
