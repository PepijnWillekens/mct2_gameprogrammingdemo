﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class CrossProduct : MonoBehaviour {

    public VectorArrow vector1;
    public VectorArrow vector2;
    public VectorArrow result;
    // Update is called once per frame
    void Update () {
        result.v = Vector3.Cross(vector1.v, vector2.v);
	}
}
