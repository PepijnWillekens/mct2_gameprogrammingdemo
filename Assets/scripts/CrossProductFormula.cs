﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CrossProductFormula : MonoBehaviour {
    public string other1 = "";
    public string other2 = "";
    public InputField A1;
    public InputField A2;
    public InputField B1;
    public InputField B2;
    public InputField R;
    public Text output;

    const string s = "A{0}*B{1} - A{1}*B{0} = {2}*{5} - {3}*{4} = {6}";
    void Update () {
        //                              0      1       2        3          4      5        6 
        output.text = string.Format(s, other1,other2, t(A1.text), t(A2.text), t(B1.text), t(B2.text), t(R.text));
	}
    string t(string s)
    {
        float f;
        if (float.TryParse(s, out f)) {
            return f.ToString("0.0#");
        }
        else
            return "...";
    }
}
