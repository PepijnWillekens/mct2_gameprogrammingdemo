﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[ExecuteInEditMode]

[RequireComponent(typeof(VectorArrow))]
public class VectorControl : MonoBehaviour
{
    VectorArrow v;
    public Vector3 editorEuler;
    public float editorLength;
    public bool updateValues = true;
    public InputField InputX, InputY, InputZ, InputL;
    public ButtonHold xp, xm, yp, ym, zp, zm;
    bool[] b = new bool[4];
    public float rotateSpeed;
    void Awake()
    {
        v = GetComponent<VectorArrow>();
    }

    float UpdateInput(InputField i, float f, int index)
    {

        if (!i || i.isFocused)
        {
            b[index] = true;
            return f;
        }
        float ft;
        if (b[index])
        {
            if (float.TryParse(i.text, out ft) && Application.isPlaying)
            {
                f = ft;
            }
            b[index] = false;
        }
        i.text = f.ToString();
        return f;
    }

    void Rotate(ButtonHold b, float x, float y, float z)
    {
        if (b && b.pressed)
            editorEuler += new Vector3(x, y, z) * Time.deltaTime * rotateSpeed;
    }

    void Update()
    {
        if (updateValues)
        {
            v.v.x = UpdateInput(InputX, v.v.x, 0);
            v.v.y = UpdateInput(InputY, v.v.y, 1);
            v.v.z = UpdateInput(InputZ, v.v.z, 2);
            v.Length = UpdateInput(InputL, v.Length, 3);

            Rotate(xp, 1, 0, 0);
            Rotate(xm, -1, 0, 0);
            Rotate(yp, 0, 1, 0);
            Rotate(ym, 0, -1, 0);
            Rotate(zp, 0, 0, 1);
            Rotate(zm, 0, 0, -1);

            if (editorEuler != Vector3.zero)
                v.Rotate(editorEuler);
            editorEuler = Vector3.zero;


            if (editorLength != 0)
                v.Length += editorLength;
            editorLength = 0;
        }
        else
        {
            InputX.text = v.v.x.ToString();
            InputY.text = v.v.y.ToString();
            InputZ.text = v.v.z.ToString();
            InputL.text = v.Length.ToString();
        }
    }
}
