﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class VectorArrow : MonoBehaviour {
    public Vector3 v;
    public Transform head;
    public Transform stick;
    Transform t ;
    public float headLength;
    public void Start()
    {
    }
    public Quaternion rotation { get { return head.localRotation; } }
    public void Rotate(Vector3 euler) {

        t = new GameObject().transform;
        t. rotation = rotation;
        t.Rotate(euler,Space.World);
        Vector3 v2;
        v2 = t.forward.normalized;
        v = v2 * v.magnitude;
        DestroyImmediate(t.gameObject);
    }
    public float Length {
        get {
            return v.magnitude;
        }
        set {
            
            v= v.normalized * Mathf.Max(0.05f, value);
        }
    }
    bool iSSet { get { return head != null && stick != null; } }
	void Update () {
        if (iSSet)
        {
            head.localPosition = v;
            stick.localPosition = Vector3.zero;
            head.LookAt(v*2);
            stick.LookAt(v*2);
            head.localPosition = v;
            if (Length >= headLength)
            {
                Vector3 zscale = stick.localScale;
                zscale.z = Length - headLength;
                stick.localScale = zscale;
                head.localScale = Vector3.one;
            }
            else {
                Vector3 zscale = stick.localScale;
                zscale.z = 0;
                stick.localScale = zscale;
                zscale = head.localScale;
                zscale.z = Length / headLength;
                head.localScale = zscale;
            }

        }
	}
}
