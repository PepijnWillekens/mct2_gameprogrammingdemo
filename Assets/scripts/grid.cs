﻿using UnityEngine;
using System.Collections;

public class grid : MonoBehaviour {
    public float lineWidth;
    public int size;
    public Material m;
    public Vector3 rot;
	void Start () {

        for (int x = -size; x <= size; x++) {

            Transform t = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            t.SetParent(transform);
            t.localScale = new Vector3(lineWidth, size * 2, lineWidth);
            t.localPosition = new Vector3(x, 0, 0);
            t.GetComponent<Renderer>().material = m;
        }
        for (int y = -size; y <= size; y++)
        {

            Transform t = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            t.SetParent(transform);
            t.localScale = new Vector3(size * 2, lineWidth,  lineWidth);
            t.localPosition = new Vector3( 0,y, 0);
            t.GetComponent<Renderer>().material = m;
        }
        transform.Rotate(rot);
    }
}
